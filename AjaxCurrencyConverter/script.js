'use strict';
/*jslint node: true*/

let inputRub = document.querySelector('#rub'),
    inputUsd = document.querySelector('#usd');

inputRub.addEventListener('input', () => {
    const request = new XMLHttpRequest();
    
    // request.open(method, url, async, login, pass);
    request.open('GET', 'database.json');
    request.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    request.send();

    /* status https://en.wikipedia.org/wiki/List_of_HTTP_status_codes */
    /* statusText */
    /* response */

    /* 
    readyState
    https://developer.mozilla.org/ru/docs/Web/API/XMLHttpRequest/readyState
    */

    /* request.addEventListener('readystatechange', () => {
        if (request.readyState === 4 && request.status === 200) {
            const data = JSON.parse(request.response);
            inputUsd.value = (+inputRub.value / data.current.usd).toFixed(2);
        } else {
            inputUsd.value = 'Something went wrong';
        }
    }); */

    request.addEventListener('load', () => {
        console.log(`
        request.status: ${request.status}
        request.readyState: ${request.readyState}
        request.statusText: ${request.statusText}
        request.response:${request.response}
        `);

        if (request.status === 200) {
            
            const data = JSON.parse(request.response);
            inputUsd.value = (+inputRub.value / data.current.usd).toFixed(2);
        } else {
            alert('hey dumb!');
            inputUsd.value = "Something went wrong";
        }
    });
});